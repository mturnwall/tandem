import React from 'react';
import { shallow } from 'enzyme';
import { Reporter } from '../components/Reporter.jsx';

const normalData = require('../../data/data-1234.json').data;

const testData = {
    mean: '49.457050',
    median: '49.000000',
    stdDev: '28.810315',
    mode: '77.000000',
};

const defaultProps = {
    data: normalData,
    type: 'mean',
};

const renderComponent = (props = {}) => {
    const mergedProps = { ...defaultProps, ...props };
    return shallow(<Reporter {...mergedProps} />);
};

describe('Reporter', () => {
    it('Reporter renders', () => {
        const subject = renderComponent();
        expect(subject.exists()).toBe(true);
    });
    it('Empty data array passed renders empty state', () => {
        const subject = renderComponent({ data: [] }).find('div');
        expect(subject.text()).toBe('No Data');
    });
    it('Display mean data', () => {
        const subject = renderComponent().find('div');
        expect(subject.text()).toBe(testData.mean);
    });
    it('Display median data', () => {
        const subject = renderComponent({ type: 'median' }).find('div');
        expect(subject.text()).toBe(testData.median);
    });
    it('Display standard deviation data', () => {
        const subject = renderComponent({ type: 'stdDev' }).find('div');
        expect(subject.text()).toBe(testData.stdDev);
    });
    it('Display mode data', () => {
        const subject = renderComponent({ type: 'mode' }).find('div');
        expect(subject.text()).toBe(testData.mode);
    });
});
