import {
    arraySort,
    calculateMean,
    calculateMedian,
    calculateMode,
    calculateStdDeviation,
} from '../helpers.js';

const normalData = require('../../data/data-1234.json').data;
const testData = {
    mean: '49.457050',
    median: '49.000000',
    stdDev: '28.810315',
    mode: '77.000000',
};
const arr = [3, 4, 1, 2, 0, 9, 7, 8, 6, 5];
const sortAsc = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
const sortDesc = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0];

describe('Helper Functions', () => {
    it('Sort array ascending', () => {
        expect(arraySort(arr)).toEqual(sortAsc);
    });
    it('Sort array descending', () => {
        expect(arraySort(arr, 'desc')).toEqual(sortDesc);
    });
    /**
     * In order to count the number of digits first need to convert to string.
     * But now the decimal point adds to the length so the length is number of
     * digitis + 1. So we use `replace()` to remove the decimal point.
     */
    describe('Calculate Mean', () => {
        it('Calculate Mean with default precision', () => {
            expect(calculateMean(normalData)).toBe(testData.mean);
        });
        it('Calculate Mean with precision parameter', () => {
            const resultLength = calculateMean(normalData, 3)
                .toString()
                .replace('.', '').length;
            expect(resultLength).toBe(5);
        });
    });
    describe('Calculate Median', () => {
        it('Calculate Median with default precision', () => {
            expect(calculateMedian(normalData)).toBe(testData.median);
        });
        it('Calculate Median with precision parameter', () => {
            const resultLength = calculateMedian(normalData, 3)
                .toString()
                .replace('.', '').length;
            expect(resultLength).toBe(5);
        });
    });
    describe('Calculate Standard Deviation', () => {
        it('Calculate Standard Deviation with default precision', () => {
            expect(calculateStdDeviation(normalData)).toBe(testData.stdDev);
        });
        it('Calculate Standard Deviation with precision parameter', () => {
            const resultLength = calculateStdDeviation(normalData, 3)
                .toString()
                .replace('.', '').length;
            expect(resultLength).toBe(5);
        });
    });
    describe('Calculate Mode', () => {
        it('Calculate Mode with default precision', () => {
            expect(calculateMode(normalData)[0]).toBe(testData.mode);
        });
        it('Calculate Mode with precision parameter', () => {
            const resultLength = calculateMode(normalData, 3)[0]
                .toString()
                .replace('.', '').length;
            expect(resultLength).toBe(5);
        });
    });
});
