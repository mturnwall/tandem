const host = 'https://private-c9ddd-tandem2.apiary-mock.com';
const path = '';

const endpoints = {
    normal: 'normal-report',
    reverse: 'reverse-report',
};

const api = (endpoint, body, method = 'GET') => {
    if (!(endpoint in endpoints)) {
        throw new Error(`The api endpoint, ${endpoint}, was not found.`);
    }
    const url = `${host}/${path}${endpoints[endpoint]}`;
    return fetch(url, {
        body,
        method,
    })
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response);
            }
            return response.json();
        })
        .catch(() => {
            // handle error
        });
};

const arraySort = (values, dir = 'asc') =>
    [...values].sort((a, b) => (dir === 'asc' ? a - b : b - a));

const calculateMean = (values, precision = 6) => {
    // can all these array checks be abstracted?
    if (!Array.isArray(values)) {
        throw new Error('calculateMean expects an array of values');
    }
    const total = values.reduce((acc, cur) => {
        return acc + cur;
    }, 0);
    return (total / values.length).toFixed(precision);
};

const calculateMedian = (values, precision = 6) => {
    if (!Array.isArray(values)) {
        throw new Error('calculateMedian expects an array of values');
    }
    const midPoint = Math.floor(values.length / 2);
    const sorted = arraySort(values);
    const median =
        sorted.length % 2 === 0
            ? (sorted[midPoint] + sorted[midPoint - 1]) / 2
            : sorted[midPoint];
    return median.toFixed(precision);
};

const calculateStdDeviation = (values, precision = 6) => {
    const mean = calculateMean(values);
    const sumOfDiffs = values.reduce((acc, cur) => {
        return acc + Math.pow(cur - mean, 2);
    }, 0);
    return Math.sqrt(sumOfDiffs / (values.length - 1)).toFixed(precision);
};

const calculateMode = (values, precision = 6) => {
    if (!Array.isArray(values)) {
        throw new Error('calculateMode expects an array of values');
    }
    let mode = [];
    let maxCount = 0;
    const counts = {};
    values.forEach(value => {
        counts[value] = (counts[value] || 0) + 1;
        if (counts[value] > maxCount) {
            maxCount = counts[value];
        }
    });
    Object.keys(counts).forEach(key => {
        if (counts[key] === maxCount) {
            mode.push(Number(key).toFixed(precision));
        }
    });
    return mode;
};

export {
    api,
    arraySort,
    calculateMean,
    calculateMedian,
    calculateMode,
    calculateStdDeviation,
};
