const theme = {
    baseColors: {
        shark: '#282c34',
        silver: '#ccc',
        white: '#fff',
    },
    headers: {
        alpha: {
            'font-size': '26px',
            'font-weight': 'bold',
            'line-height': '1.2',
            'padding-bottom': '0.2em',
            'border-bottom': '1px solid #999',
        },
        bravo: {
            'font-size': '18px',
            'font-weight': '700',
            'line-height': '1.2',
            margin: '0 0 12px',
        },
    },
};

export { theme };
