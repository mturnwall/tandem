import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { string } from 'prop-types';
import { api } from '../helpers.js';
import { SectionTitle } from './SectionTitle.jsx';
import { Reporter } from './Reporter.jsx';
import { Form } from './ReporterForm.jsx';
import { Loading } from './Loading.jsx';

const StyledDashboard = styled.section`
    color: ${({ theme }) => theme.baseColors.white};
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 20px;
    justify-items: center;
`;
const StyledHeader = styled.header`
    grid-column: 1 / 5;
    text-align: center;
    margin-bottom: 20px;
`;
const StyledLoader = styled(Loading)`
    grid-column: 1 / 5;
`;

const propTypes = {
    label: string.isRequired,
};

/**
 * This would be tested with an integration test suite.
 * I just focused on unit tests for this exercise.
 */
const ReportDashboard = ({ label, type }) => {
    const [data, setData] = useState([]);
    const [dataType, setDataType] = useState('normal');
    /**
     * This isn't being cached because I'm simulating actually refreshing the data.
     * A refactor could be caching this data and making a request to the API to see
     * if the data has been updated since last fetch. If not just use cache data.
     */
    const getData = type => api(type).then(response => setData(response.data));
    useEffect(() => {
        getData(dataType);
    }, [dataType]);

    const handleClick = evt => {
        evt.preventDefault();
        setDataType(dataType === 'normal' ? 'reverse' : 'normal');
    };
    const handleSubmit = formData => {
        const newValues = formData.newValue
            .split(/\s*,\s*/)
            .map(value => Number(value));
        setData([...data, ...newValues]);
    };
    return (
        <StyledDashboard>
            <StyledHeader>
                <SectionTitle>{label}</SectionTitle>
                <button onClick={handleClick}>Refresh Data</button>
            </StyledHeader>
            {data.length ? (
                <>
                    <Reporter label="Mean" data={data} type="mean" />
                    <Reporter label="Median" data={data} type="median" />
                    <Reporter label="Std Deviation" data={data} type="stdDev" />
                    <Reporter label="Mode" data={data} type="mode" />
                    <Form id="reporter-form" handleSubmit={handleSubmit} />
                </>
            ) : (
                <StyledLoader />
            )}
        </StyledDashboard>
    );
};

ReportDashboard.propTypes = propTypes;

export { ReportDashboard };
