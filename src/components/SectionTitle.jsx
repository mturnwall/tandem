import React from 'react';
import styled from 'styled-components';
import { oneOf } from 'prop-types';

/**
 * This gives an example of how a component can be the themed.
 * The component has no actually styling. The styling is controlled
 * through the theme object (theme.js) that is made global using ThemeProvider.
 */
const Tag = styled.div`
    ${({ theme: { headers }, level }) => {
        return Object.keys(headers[level]).map(key => ({
            [key]: headers[level][key],
        }));
    }}
`;

const propTypes = {
    level: oneOf(['alpha', 'bravo', 'charlie', 'delta', 'echo']),
    tag: oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']),
};
const defaultProps = {
    level: 'alpha',
    tag: 'h1',
};

const SectionTitle = ({ level, tag, children, ...props }) => (
    <Tag as={tag} level={level} {...props}>
        {children}
    </Tag>
);

SectionTitle.propTypes = propTypes;
SectionTitle.defaultProps = defaultProps;

export { SectionTitle };
