import React from 'react';
import styled from 'styled-components';
import { string, func } from 'prop-types';

const FormControl = styled.div`
    display: block;
    margin: 10px 0;
`;
const StyledLabel = styled.label`
    display: block;
    margin-bottom: 4px;
`;

const propTypes = {
    handleChange: func,
    label: string.isRequired,
    name: string,
    placeholder: string,
    value: string,
};

const defaultProps = {
    placeholder: '',
    value: '',
};

const Input = ({ name, placeholder, value, label, handleChange }) => {
    return (
        <FormControl>
            <StyledLabel htmlFor="">{label}</StyledLabel>
            <input
                type="text"
                value={value}
                onChange={evt => handleChange(evt.target)}
                name={name}
                placeholder={placeholder}
            />
        </FormControl>
    );
};

Input.propTypes = propTypes;
Input.defaultProps = defaultProps;

export { Input };
