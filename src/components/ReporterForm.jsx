import React, { useState } from 'react';
import { string, func } from 'prop-types';
import { Input } from './Input.jsx';

const propTypes = {
    handleSubmit: func.isRequired,
    id: string,
};

const defaultProps = {
    id: `form-${Math.random().toString(16).substring(2)}`,
};

const Form = ({ handleSubmit, id }) => {
    const [formData, setFormData] = useState({});
    const handleChange = formControl => {
        setFormData({
            ...formData,
            [formControl.name]: formControl.value,
        });
    };
    const sendSubmit = evt => {
        evt.preventDefault();
        handleSubmit(formData);
        setFormData({});
    };
    return (
        <form id={id} onSubmit={sendSubmit}>
            <Input
                label="Add Data"
                value={formData.newValue || ''}
                name="newValue"
                handleChange={handleChange}
            />
            <button type="submit">Submit</button>
        </form>
    );
};

Form.propTypes = propTypes;
Form.defaultProps = defaultProps;

export { Form };
