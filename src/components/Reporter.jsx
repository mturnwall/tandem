import React from 'react';
import { arrayOf, number, oneOf, string } from 'prop-types';
import {
    calculateMean,
    calculateMedian,
    calculateMode,
    calculateStdDeviation,
} from '../helpers.js';
import { SectionTitle } from './SectionTitle.jsx';

const calculations = {
    mean(values) {
        return calculateMean(values);
    },
    median(values) {
        return calculateMedian(values);
    },
    mode(values) {
        return calculateMode(values).join(' ');
    },
    stdDev(values) {
        return calculateStdDeviation(values);
    },
};

const propTypes = {
    data: arrayOf(number),
    label: string,
    type: oneOf(['mean', 'median', 'mode', 'stdDev']),
};

const defaultProps = {
    data: [],
    label: 'Mean',
};

/**
 * I tried using React.memo to see if it improved performance because
 * of the large datasets. I couldn't discern any noticeable differene though. This is
 * something that I would benchmark if it was going to production.
 */
const Reporter = ({ data, label, type }) => {
    return (
        <section>
            <SectionTitle level="bravo">{label}</SectionTitle>
            <div>
                {data.length !== 0 ? calculations[type](data) : 'No Data'}
            </div>
        </section>
    );
};

Reporter.propTypes = propTypes;
Reporter.defaultProps = defaultProps;

const MemoizedReporter = React.memo(Reporter, (prevProps, nextProps) => {
    return prevProps.data === nextProps.data;
});

export { MemoizedReporter as Reporter };
