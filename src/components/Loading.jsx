import React from 'react';
import styled from 'styled-components';
import logo from '../logo.svg';

const StyledLoader = styled.div`
    text-align: center;
`;
const AnimatedImage = styled.img`
    display: block;
    height: 40vmin;
    pointer-events: none;
    @media (prefers-reduced-motion: no-preference) {
        animation: App-logo-spin infinite 2s linear;
    }
    @keyframes App-logo-spin {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(360deg);
        }
    }
`;

const Loading = ({ className }) => {
    return (
        <StyledLoader className={className}>
            Loading data...
            <AnimatedImage src={logo} alt="logo" />
        </StyledLoader>
    );
};

export { Loading };
