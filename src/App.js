import React from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { theme } from './theme.js';
import { ReportDashboard } from './components/ReportDashboard.jsx';

const PageWrapper = styled.div`
    background-color: ${({ theme }) => theme.baseColors.shark};
    min-height: 100vh;
    width: 800px;
    margin: 0 auto;
    font-size: 16px;
`;

const PageHeader = styled.header`
    font-size: calc(10px + 2vmin);
    color: white;
    text-align: center;
`;

function App() {
    return (
        <ThemeProvider theme={theme}>
            <PageWrapper className="App">
                <PageHeader className="App-header">
                    <h1>Tandem Diabetics Reporter</h1>
                </PageHeader>
                <ReportDashboard label="Report Data" />
            </PageWrapper>
        </ThemeProvider>
    );
}

export default App;
